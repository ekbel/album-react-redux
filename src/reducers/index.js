import {
    PRODUCTS_BY_CATEGORY,
    PRODUCTS_BY_ALL,
    PRODUCT_VIEW,
    ADD_TO_CART,
    REMOVE_FROM_CART,

    PAGINATION
} from '../actions';
import { combineReducers } from 'redux';
import json_products from '../data/cars.json';

function productsFilter(state = [], action){
    switch (action.type) {
      case PRODUCTS_BY_CATEGORY:
        return findProductsByCategory(action)
      case PRODUCTS_BY_ALL:
        return json_products
    default:
        return state
  }
}

function productView(state = [], action){
    switch (action.type) {
      case PRODUCT_VIEW:
        return getProductData(action)
    default:
        return state
  }
}

function addToCart(state = [], action){
    switch (action.type) {
      case ADD_TO_CART:
        return [
                ...state,
                getProductData(action)
        ]
      case REMOVE_FROM_CART:
        return state.filter( item => item[0].id !== action.id );
      default:
            return state
  }
}





function paginationProducts(state = [], action){
    switch (action.type) {
      case PAGINATION:
        return getpaginationProducs(action)
    default:
        return state
  }
}

function getpaginationProducs(action){
    let pageNum = action.pagination.pageNum;
    let categoryName = action.pagination.categoryName;
    let min = pageNum > 1 ? (pageNum * 10) - 11 : 0;

    let filterObj = categoryName !== undefined ? findProductsByCategoryInternal(categoryName) : json_products;
    return filterObj.slice(min, min + 10);
}

function getProductData(action){
    return json_products.filter((product) => product.id === action.id);
}


function findProductsByCategory(action) {
    return json_products.filter((product) => {
            if(product.category.toLowerCase() === action.category.toLowerCase())
            {
                return product.category;
            }
        });
}

// help function inside the reducer, with no access to action object.
function findProductsByCategoryInternal(categoryName) {
    switch (categoryName) {
        case "vr":
            categoryName =  "Virtual Reality";
            break;
        case "audio":
            categoryName =  "TV & Audio";
            break;
        default:
            break;
    }

    return json_products.filter((product) => {
            if(product.category.toLowerCase() === categoryName.toLowerCase())
            {
                return product.category;
            }
        });
}

const rootReducer = combineReducers({
      productsFilter,
      productView,
      addToCart,
      paginationProducts
});

export default rootReducer;

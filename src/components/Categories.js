import React, { Component } from 'react';
import Header from './header/Header';
import CategoryList from './CategoryList';
import PaginationCategories from './PaginationCategories';


var mainBar ={
  "font-size":"15px",
  "font-weight": "bold",
  "height": "50px",
  "padding-left": "10%",
  "border-top": "1px solid hsl(0, 0%, 100%)",
  "color": "#090909",
  "padding-top": "1%",
}

var paginationContainer ={
  "text-align": "center",
  "margin-bottom": "5%"
}


class Categories extends Component {

  getCategoryName(){

      let categoryName = "categories";

      //eslint-disable-next-line
      if(location.pathname === "/categories")
      {
           categoryName = "categories";
      }
      else{
          //eslint-disable-next-line
          let str = location.pathname.split("/");
          let pathName = str[str.length-1];
          categoryName = pathName;
       }

       return categoryName;
  }

  render() {
    return (
      <div>
        <Header />
        <main>
          <div style={mainBar}>
              <h2>{this.getCategoryName()} > </h2>
          </div>
          <div className="row">
              <div className="col-lg-1">
              </div>
              <div className="col-lg-10">
                <CategoryList />
              </div>
          </div>
          <div className="row">
              <div className="col-lg-12">
                  <div style={paginationContainer}>
                      <PaginationCategories />
                  </div>
              </div>
          </div>
        </main>
      </div>
    );
  }
}

export default Categories;

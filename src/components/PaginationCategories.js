import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Pagination} from 'react-bootstrap';
import {pagination} from '../actions';
class PaginationCategories extends Component {

  constructor(){
      super();

      this.state = {
          activePage: 1
      }
  }

  length() {
        const { productsFilter } = this.props;

        let productmodulo = (productsFilter.length + 1) % 10;
        if(productmodulo === 0 )
            return productsFilter.length / 10;
        else
            return (productsFilter.length / 10) + 1;

    }

    pageClick(event){
        this.setState({
            activePage: parseInt(event.target.text, 10)
        });


        //eslint-disable-next-line
        const pathName = location.pathname;
        if(pathName === "/categories"){
            this.props.pagination(parseInt(event.target.text, 10))
        }
        else{
             let str = pathName.split("/");
             let categoryName = str[str.length-1];
             this.props.pagination(parseInt(event.target.text, 10), categoryName)
        }

    }

  render() {


    return (

        <div id="pagination">
            <Pagination
                activePage={this.state.activePage}
                bsSize="medium"
                items={this.length()}
                onClick={event => this.pageClick(event)}
            />
        </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
       productsFilter: state.productsFilter,
       paginationProducts: state.paginationProducts
    }
}

export default connect(mapStateToProps, {pagination})(PaginationCategories);

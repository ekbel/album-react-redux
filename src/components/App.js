import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../styles/mystyle.css';
import Header from './header/Header';
import img1 from '../images/bugati_chiron.png';
import Footer from './Footer';

var bgHomepage= {
    "width": "100%",
    "height": "550px",
    "padding-top": "3%",
    "background-repeat": "no-repeat",
    "background-image": "url(../images/background.jpg)",
    "background-size": "cover",
    "background-position": "center center"
}
var hompageSale ={

  "font-family": "Georgia, 'Times New Roman', Times, serif",
  "font-size": "large",
  "text-align": "center",
  "padding-top": "10%",
}

var largeHeading ={
  "font-size":"50px"
}

var butFont ={

  "font-family":"'Roboto',sans-serif"
}



class App extends Component {
  render() {
    return (
      <div className="App" id="App">
        <Header />
        <main>
          <div style={bgHomepage}>
              <div className="col-md-6">
                  <div className="pull-right">
                      <img alt="iphone" src={img1}/>
                  </div>
              </div>
              <div className="col-md-6">
                  <div className="pull-left" style ={hompageSale}>
                      <h2 style={largeHeading}>Welcome to Ekbel first ReactJS app !</h2>

                      <Link style={butFont} className="btn btn-lg btn-primary" to="/categories">All Top Cars</Link>
                      <br />
                  </div>
              </div>
          </div>
        </main>
        <Footer />
      </div>
    );
  }
}

export default App;

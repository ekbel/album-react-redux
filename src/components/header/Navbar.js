import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import '../../styles/app.css';
import {productsByCategory} from '../../actions';

var mainBg ={
  "background": "#008975",
}

var navbarUl ={
  "font-size": "15px",
  "letter-spacing": "1px",
  "margin-bottom":"0"
}
class NavBar extends Component {

 menuFilter(event){
    this.props.productsByCategory(event.target.text);
 }

  render() {
    return (
      <div style={mainBg}>
            <div className="row">
                <div className="col-md-12">
                    <nav  id="nav-bar" className="text-center">
                        <ul style={navbarUl} className="list-inline">
                            <li>
                                <Link
                                onClick={ event => this.menuFilter(event)}
                                to="/categories/sedan">SEDAN</Link>
                            </li>
                            <li>
                                <Link
                                 onClick={ event => this.menuFilter(event)}
                                 to="/categories/suv">SUV</Link>
                            </li>
                            <li>
                                <Link
                                 onClick={ event => this.menuFilter(event)}
                                to="/categories/citycars">CITYCARS</Link>
                            </li>
                            <li>
                                <Link
                                 onClick={ event => this.menuFilter(event)}
                                to="/categories/pickup">PICKUP</Link>
                            </li>
                            <li>
                                <Link
                                 onClick={ event => this.menuFilter(event)}
                                to="/categories/compactcars">COMPACTCARS</Link>
                            </li>
                            <li>
                                <Link
                                 onClick={ event => this.menuFilter(event)}
                                to="/categories/businesscars">BUSINESSCARS</Link>
                            </li>

                        </ul>
                    </nav>
                </div>
            </div>
      </div>
    );
  }
}


export default connect(null, {productsByCategory})(NavBar);

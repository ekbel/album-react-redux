import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Form, FormControl, FormGroup} from 'react-bootstrap';
import {productsByCategory} from '../../actions';
 var searchBtt={
   "height": "28px",
   "border-radius": "0 2px 2px 0",
   "border": "1px solid #00D1B2",
   "background":"#00D1B2",
   "padding": "2px",
   "width": "30px"
 }

var searchInput= {
  "width": "200px",
  "height": "20px",
  "border-radius": "2px 0 0 2px",
  "border": "1px solid #00D1B2",
  "padding": "13px",

}

class Search extends Component {

    constructor() {
        super();
        this.state = {
            searchTerm: ""
        }
    }


  render() {
    return (
        <div>
            <Form inline>
                <FormGroup>
              <span><FormControl
                        style={searchInput}
                         placeholder="Search..." />
              </span><span>
                     <div
                        style={searchBtt}
                        className="btn btn-lg btn-primary"
                         ><span class="glyphicon glyphicon-search"></span></div>
              </span>

                </FormGroup>

            </Form>
        </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
       addToCart: state.addToCart
    }
}

export default connect(mapStateToProps, { productsByCategory })(Search);

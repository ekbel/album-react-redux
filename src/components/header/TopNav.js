import React, { Component } from 'react';
import {connect} from 'react-redux';
import {productsByCategory} from '../../actions';

var topNav = {
  "border-bottom": "1px solid hsl(0, 0%, 87%)",
  "background-color": "#f5f5f5",
}
var topnavHeadline ={
  "padding-top": "1%",
  "padding-left": "5%",
  "padding-bottom": "10px"
}

var beforeBorder = {
  "content":" '|'",
  "color": "hsl(180, 12%, 81%)",
  "display": "inline-block",
  "margin": "0 1em"

}


class TopNav extends Component {
    menuFilter(event){
        this.props.productsByCategory(event.target.text);
    }


  render() {
    return (
        <div style={topNav}>
            <div className="row">
                <div className="col-md-8">
                    <div style ={topnavHeadline}>
                        <span>Wellcome To Top Car Collection !</span>
                    </div>
                </div>
                <div style ={topnavHeadline} className="col-md-4">
                    <ul className="list-inline text-center">
                        <li>Home</li>
                        <li style={beforeBorder}>Account</li>
                        <li style={beforeBorder}>About us</li>
                    </ul>
                </div>
            </div>
        </div>
    );
  }
}


export default connect(null, {productsByCategory})(TopNav);

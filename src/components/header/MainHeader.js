import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {connect} from 'react-redux';
import {Glyphicon} from 'react-bootstrap';
import Search from './Search';
import 'roboto-npm-webfont';


var myLogo = {
"margin-top": "12%",
}
var mySearch = {
  "padding": "5%",
}

var myWishlist ={

   "margin-top": "11%",
   "font-size": "18px",
   "font-family": "'Roboto', sans-serif"

}

class MainHeader extends Component {

 calculateCart(){
     let priceNum = 0;
     let carSum = 0;
    this.props.addToCart.map( (product, index) => {
      //  priceNum += parseInt(product[0].price, 10);
        carSum = this.props.addToCart.length;
    });

     return ` ${priceNum} `,` ${carSum} `;
 }



  render() {
    return (
        <div>
            <div className="row">
                <div className="col-md-3">
                    <div style={myLogo} className="text-center">
                        <h1><Link to="/">TOP CAR COLLECTION</Link></h1>
                    </div>
                </div>
                <div className="col-md-6">
                    <div style={mySearch} className="text-center">
                        <Search />
                    </div>
                </div>
                <div className="col-md-3">
                    <div style={myWishlist} className="text-center">
                        <ul className="list-inline">
                            <li>
                                <Link
                                 to="/cart"
                                >
                                <span class="glyphicons glyphicons-heart"></span>
                                    <Glyphicon glyph="heart" />
                                    {this.calculateCart()}
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
       addToCart: state.addToCart,
     }
}

export default connect(mapStateToProps, null)(MainHeader);

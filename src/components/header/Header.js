import React, { Component } from 'react';
import TopNav from './TopNav';
import MainHeader from './MainHeader';
import NavBar from './Navbar';
import 'roboto-npm-webfont';


var maxHeader ={
  'max-height': '250px',
}

class Header extends Component {
  render() {
    return (
      <header style={maxHeader}>
        <TopNav />
        <MainHeader />
        <NavBar />
      </header>
    );
  }
}

export default Header;

import React, { Component } from 'react';
import {connect} from 'react-redux';
import Header from './header/Header';
import {removeFromCart} from '../actions';
import img from '../images/luxury.png';



var cartIimage={
    "max-width": "100px",
    "max-height": "80px"
};
var removeProduct={
    "cursor": "pointer",
    "color":"red",
    "font-size": "18px"
};

var textStyle={
  "padding-top":"10%",
  "position":"center",
  "margin-left":"40%",
  "font-size":"18px"
}

var wishlistTable={
 "margin-top":"10%",
 "background-color":"#f9f9f9",

}

class Cart extends Component {

  constructor(){
    super();
    this.state = {
      price: 0
    }
  }

  componentDidMount(){
    this.calculatePrice()
  }

  calculatePrice() {
    let totalPrice = 0;
    this.props.addToCart.map( (product, index) => {
       totalPrice += product[0].price;
    });

    this.setState({
      price: totalPrice
    })
  }

  removeProduct(id){
      this.props.removeFromCart(id);
      setTimeout( () => {
        this.calculatePrice();
      },100)
  }


  render() {

    return (
      <div id="cart">
        <Header />
        <main>
          <div>
          {
              this.props.addToCart.length > 0
              ?
                    <div className="col-md-offset-2 col-md-8">
                        <table style={wishlistTable} className="table">
                          <thead>
                            <tr>
                              <th>Image:</th>
                              <th>Name:</th>
                              <th>Price</th>
                              <th>Remove</th>
                            </tr>
                          </thead>
                          <tbody>
                          {
                              this.props.addToCart.map( (product, index) => {
                                  return (
                                          <tr key={product[0].id}>
                                            <td><img style={cartIimage} src={product[0].image} /></td>
                                            <td>{product[0].name}</td>
                                            <td>{product[0].price} $</td>
                                            <td style={removeProduct} onClick={ () => this.removeProduct(product[0].id) }>X</td>
                                          </tr>
                                  )
                              })
                          }
                          </tbody>
                          <tfoot>
                              <tr>
                                <td>Total Amount: {this.props.addToCart.length}</td>
                                <td></td>
                                <td></td>
                                <td>Total Price: {this.state.price}</td>
                              </tr>
                          </tfoot>
                        </table>
                    </div>
              :
                    <div style={textStyle}>
                      <h2>Your wishlist is Empty...</h2>
                    </div>
          }

          </div>
        </main>

      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
       addToCart: state.addToCart
    }
}

export default connect(mapStateToProps, {removeFromCart})(Cart);

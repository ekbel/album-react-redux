import React, { Component } from 'react';
import {Button, Glyphicon} from 'react-bootstrap';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import Notifications, {notify} from 'react-notify-toast';
import {viewProduct, addToCart} from '../actions';
 import Header from './header/Header';

var mainbannerProduct={
  "border-top": "1px solid hsl(0, 0%, 100%)",
  "color": "black",
  "font-weight": "bold",
  "padding-top": "2%"
}
var productviewDesc ={
  "font-weight": "normal",
  "text-align": "left"
}
var productCateg = {
  "font-weight":"bold",
}

var productTitle ={
  "font-weight":"bold",
  "color":"#333"
}

var productDesc ={
  "color":"#95979b"
}

var butFont ={

  "font-family":"'Roboto',sans-serif"
}

class ProductView extends Component {

    componentWillMount(){
        if(this.props.productView.length === 0)
        {
            //eslint-disable-next-line
            let str = location.pathname.split("/");
            this.props.viewProduct(parseInt(str[str.length-1], 10));
            console.log();
        }
    }

  addToCart(){
    this.props.addToCart(this.props.productView[0].id);
    notify.show(`${this.props.productView[0].name} Was added to Wishlist`, 'success' );
  }

  render() {
    return (
      <div className="App">
        <Header />
        <main>
         <Notifications />
            <div style ={mainbannerProduct} className="text-center">
                <div className="row">
                    <div className="col-lg-4">
                        <div>

                            <img alt="product" src={  this.props.productView.length > 0
                              ?
                                   this.props.productView[0].image
                              :
                                    "loading..."}/>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div style={productviewDesc}>
                            <div style = {productCateg}>
                                <span>
                                    {
                                        this.props.productView.length > 0
                                        ?
                                             this.props.productView[0].category
                                        :
                                              "loading..."
                                    }
                                </span>
                            </div>
                            <div style ={productTitle}>
                                <h2>
                                    {
                                        this.props.productView.length > 0
                                        ?
                                             this.props.productView[0].name
                                        :
                                              "loading..."
                                    }
                                </h2>
                            </div>
                            <div style = {productDesc}>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                <p>Lorem Ipsum has been the industry standard dummy text ever since the 1500s,
                                 when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                 <ul>
                                    <li>Lorem Ipsum is not simply random text. </li>
                                    <li>Lorem Ipsum There are many variations of passages</li>
                                    <li>Lorem Ipsum It is a long established fact that a reader</li>
                                    <li>Lorem Ipsum when an unknown printer took a galley of type and scrambled</li>
                                 </ul>
                                 <p>Lorem Ipsum It is a long established fact that a reader will be distracted by
                                  the readable content of a page when looking at its layout.</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4">


                            <div className="text-center">
                                <Button
                                 style ={butFont}
                                className="btn btn-lg btn-primary"
                                onClick={this.addToCart.bind(this)}
                                bsSize="large">
                                <span class="glyphicons glyphicons-heart"></span>
                                    <Glyphicon glyph="heart" />

                                   Add to Wishlist</Button>
                                {
                                    this.props.addToCart.length > 0
                                    ?
                                        <Link
                                            to="/cart"
                                        >
                                                <Glyphicon glyph="shopping-cart" />
                                                Go to Cart
                                        </Link>
                                    :
                                        <div></div>
                                }
                            </div>
                        </div>
                    </div>

            </div>
        </main>
      </div>
    );
  }
}


const mapStateToProps = (state) => {
    return {
       productView: state.productView,
       addToCart: state.addToCart

    }
}

export default connect(mapStateToProps, {viewProduct ,addToCart})(ProductView);

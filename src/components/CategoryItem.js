import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import Notifications, {notify} from 'react-notify-toast';
import img from '../images/luxury.png';
import products from '../data/cars.json';
import {addToCart, viewProduct} from '../actions';

var productContainer={
  "background-color": "hsl(0, 0%, 100%)",
  "border": "1px solid hsl(0, 0%, 93%)",
  "height": "370px",
  "padding": "7%",
  "margin-top": "5%",
  "font-family": "Georgia, 'Times New Roman', Times, serif",
}

var productName ={
  "font-size": "20px",
  "letter-spacing": "1px",
  "color": "#3273dc",
  "font-weight": "bold",
  "min-height":"70px",
}

var productImage ={
  "width": "100%",
  "min-height":"200px",
  "padding-bottom": "10px",

}
var productPrice ={
  "font-size":"17px",
  "font-family": "Arial, Helvetica, sans-serif",
  "font-weight": "bold"

}
var productView = {
  "padding-top": "10px",

}
var productCurrency = {
  "font-size": "10px",
  "font-family": "Arial, Helvetica, sans-serif",
}

class CategoryItem extends Component {

  constructor(){
    super();
    this.show = notify.createShowQueue();
  }


  addToCart(){
    this.props.addToCart(this.props.id);
    this.show(`${this.props.name} Was added to wishlist`, 'success' );
  }

  render() {
    const product = products.find(product => product.id === this.props.id);

    return (
      <div >
       <Notifications />
        <li>
            {
              <div style={productContainer}>
                  <div style={productName} className="text-center">
                      <p>{this.props.name}</p>
                  </div>
                  <div style={productImage} className=" text-center">
                  <Link
                        to={`/product/${this.props.name}/${this.props.id}`}
                        onClick={() => this.props.viewProduct(this.props.id)}
                  >

                   <img alt={img} src={product.image}/>
                  </Link>

                  </div>
                  <div  style ={productPrice} className=" text-center">
                      <p>
                        <span>{this.props.price}</span>
                        <span style={productCurrency} >{this.props.currency}</span>
                        <span>  {this.props.category}</span>
                      </p>
                  </div>
                  <div style ={productView} className="text-center">
                      <span>
                      <Link
                            to={`/product/${this.props.name}/${this.props.id}`}
                            onClick={() => this.props.viewProduct(this.props.id)}
                      >
                            View Car
                      </Link>
                      </span>

                      <button className={`button is-pulled-right ${(this.addToCart.bind(this)) ? 'is-info' : 'is-primary'}`}
                        onClick={ this.addToCart.bind(this)} >
                        <i className="fa fa-heart" aria-hidden="true"></i>
                      </button>


                  </div>
              </div>
            }
        </li>
      </div>
    );
  }
}



export default connect(null, { addToCart, viewProduct })(CategoryItem);

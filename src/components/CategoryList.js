import React, { Component } from 'react';
import {connect} from 'react-redux';
import {getAllProducts, productsByCategory} from '../actions';
import CategoryItem from './CategoryItem';
class CategoryList extends Component {



    componentWillMount(){

      //eslint-disable-next-line
      if(location.pathname === "/categories")
      {
          this.props.getAllProducts();
      }
      else{
            //direct access from url to specific category
            if(this.props.productsFilter.length === 0)
            {
                //eslint-disable-next-line
                let str = location.pathname.split("/");
                let categoryName = str[str.length-1];
                switch (categoryName) {
                    case "suv":
                        categoryName =  "suv";
                        break;
                    case "pickup":
                        categoryName =  "pickup";
                        break;
                    default:
                        break;
                }
                this.props.productsByCategory(categoryName);
            }

       }


  }

  populateProduct() {
    const {paginationProducts, productsFilter} = this.props;
    let flag = false;
    if(paginationProducts.length > 0 && productsFilter.length > 0){
        //check if the category changed, if it does
        //the data should be taken from the product filter.
        //else it will be taken from the paginationProducts
        //eslint-disable-next-line
        if(paginationProducts[0].category === productsFilter[0].category || location.pathname === "/categories"){
            return true;
        }
    }
    return flag;
  }

  render() {
    return (
      <div id="category-list">
        <ul>

                {

                    this.populateProduct()
                    ?
                        this.props.paginationProducts.map( (product) => {
                            return (
                                <div className="col-md-3">
                                    <CategoryItem
                                        key={product.id}
                                        id={product.id}
                                        name={product.name}
                                        image={product.image}
                                        price={product.price}
                                        category={product.category}
                                        currency={product.currency}
                                    />
                                </div>
                            )
                        })
                    :
                        this.props.productsFilter.slice(0, 9).map( (product) => {
                            return (
                                <div className="col-md-3">
                                    <CategoryItem
                                        key={product.id}
                                        id={product.id}
                                        name={product.name}
                                        image={product.image}
                                        price={product.price}
                                        category={product.category}
                                        currency={product.currency}
                                    />
                                </div>
                            )
                        })
                }
        </ul>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
       productsFilter: state.productsFilter,
       paginationProducts: state.paginationProducts
    }
}

export default connect(mapStateToProps, { getAllProducts, productsByCategory })(CategoryList);

export const LOGIN_USER_JSON = "LOGIN_USER_JSON";
export const PRODUCTS_BY_CATEGORY = "PRODUCTS_BY_CATEGORY";
export const PRODUCTS_BY_ALL = "PRODUCTS_BY_ALL";
export const PRODUCT_VIEW = "PRODUCT_VIEW";
export const ADD_TO_CART = "ADD_TO_CART";
export const REMOVE_FROM_CART = "REMOVE_FROM_CART";
export const PAGINATION = "PAGINATION";
export const MENU_CATEGORY = "MENU_CATEGORY";

export function productsByCategory(category){
    return {
        type: PRODUCTS_BY_CATEGORY,
        category
    }
}

export function getAllProducts(){
    return {
        type: PRODUCTS_BY_ALL
    }
}

export function viewProduct(id){
    return {
        type: PRODUCT_VIEW,
        id
    }
}

export function addToCart(id){
    return {
        type: ADD_TO_CART,
        id
    }
}


export function removeFromCart(id){
    return {
        type: REMOVE_FROM_CART,
        id
    }
}


export function pagination(pageNum, categoryName){
        return {
            type: PAGINATION,
            pagination: {
                pageNum,
                categoryName
            }
        }
}

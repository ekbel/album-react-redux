import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import App from './components/App';
import Categories from './components/Categories';
import ProductView from './components/ProductView';
import Cart from './components/Cart';
import rootReducer from './reducers'


let store = createStore(rootReducer);
store.subscribe(() => console.log('store subscribe', store.getState()))

ReactDOM.render(
      <Provider store={store}>
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={App} />
                <Route path="/categories" component={Categories} />
                <Route path="/product" component={ProductView} />
                <Route path="/cart" component={Cart} />
            </Switch>
        </BrowserRouter>
    </Provider>
    ,
document.getElementById('root'));
